import 'semantic-ui-css/semantic.min.css';
import { Container, Divider, Header, Message } from 'semantic-ui-react';
import { useState } from 'react';
import { getUsers, postUser } from './helpers';
import { Buttons } from './Buttons';
import { UsersTable } from './UsersTable';

function App() {
  const [users, setUsers] = useState([]);
  const [usersError, setUsersError] = useState(null);

  const handleGetUsers = async () => {
    try {
      const newUsers = await getUsers();
      setUsers(newUsers);
      setUsersError(null);
    } catch (e) {
      setUsers([]);
      setUsersError(e);
    }
  };

  const handleAddUserForm = async (e) => {
    e.preventDefault();
    const newUserName = e.target.name.value;

    try {
      await postUser(newUserName);
      await handleGetUsers();
    } catch (e) {
      setUsers([]);
      setUsersError(e);
    }
  };

  return (
    <Container>
      <Divider hidden />

      <Header textAlign='center' as='h1'>
        CORS for thought
      </Header>

      <Divider hidden />

      <Buttons handleGetUsers={handleGetUsers} handleAddUserForm={handleAddUserForm} />

      <Divider hidden />

      {usersError && (
        <Message error>
          Error: {usersError.message}
          <br />
          <br />
          See console for more info.
        </Message>
      )}

      <Divider hidden />

      {users.length > 0 && <UsersTable users={users} />}
    </Container>
  );
}

export default App;
