import { Button, Divider, Form } from 'semantic-ui-react';

export const Buttons = ({ handleGetUsers, handleAddUserForm }) => (
  <>
    <Button onClick={handleGetUsers}>Get Users</Button>

    <Divider hidden />

    <Form onSubmit={handleAddUserForm}>
      <Form.Group>
        <Form.Input id='name' name='name' placeholder='Add User' />
        <Form.Button type='submit'>Submit</Form.Button>
      </Form.Group>
    </Form>
  </>
);
