export const getUsers = async () => {
  return await fetch('http://localhost:3000/users', {
    credentials: 'include',
  }).then((response) => {
    if (response.type === 'opaque') {
      console.log(response);
      throw new Error("Opaque response received - JS can't access the data");
    }
    return response.json();
  });
};

export const postUser = async (name) => {
  return fetch('http://localhost:3000/users', {
    method: 'POST',
    body: JSON.stringify({ name }),
    credentials: 'include',
    headers: {
      'x-custom-header': '1234',
      'Content-Type': 'application/json',
    },
  });
};
