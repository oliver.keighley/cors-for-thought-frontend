import { Table } from 'semantic-ui-react';

export const UsersTable = ({ users }) => (
  <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>ID</Table.HeaderCell>
        <Table.HeaderCell>Name</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {users.map((user) => (
        <Table.Row key={user.id}>
          <Table.Cell>{user.id}</Table.Cell>
          <Table.Cell>{user.name}</Table.Cell>
        </Table.Row>
      ))}
    </Table.Body>
  </Table>
);
