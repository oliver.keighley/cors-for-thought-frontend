Run the server on http://localhost:3000 - https://gitlab.com/oliver.keighley/cors-for-thought-server

Run the frontend on http://localhost:3002 - https://gitlab.com/oliver.keighley/cors-for-thought-frontend

Use the buttons in the UI to make CORS requests to the server

helpers.js contains the fetch requests which you can mess around with settings on to simulate different scenarios

eg, with/without credentials, no-cors mode, custom headers

users router on the server handles the requests and has hard coded cors response headers on main

the post request will always respond the set a cookie in the browser, but depending on CORS headers it might not save
